import {
  Button,
  Text,
  Image,
  Container,
  Box,
  BackgroundImage,
  Center,
  Highlight,
  Title,
  Flex,
  Card,
  TextInput,
  Textarea,
} from "@mantine/core";
import { useForm } from "@mantine/form";
import HeaderMenu from "@/components/HeaderMenu/HeaderMenu";
import FooterMenu from "@/components/FooterMenu/FooterMenu";

import { Carousel } from "@mantine/carousel";
import { useRouter } from "next/router";

export default function HomePage() {
  const pageStyle = {
    backgroundColor: "#FFFFFF",
  };
  const cardStyle = {
    backgroundColor: "#EC7A71",
  };
  const cardStyle2 = {
    backgroundColor: "#699971",
  };
  const cardStyle3 = {
    backgroundColor: "#5596C4",
  };

  const form = useForm({
    initialValues: {
      email: "",
      name: "",
      message: "",
      termsOfService: false,
    },

    validate: {
      email: (value) => {
        if (!value) {
          return "This field is required";
        }
        return /^\S+@\S+$/.test(value) ? null : "Invalid email address";
      },
      name: (value) => {
        if (!value) {
          return "This field is required";
        }
      },
      message: (value) => {
        if (!value) {
          return "This field is required";
        }
      },
    },
  });
  const router = useRouter();

  const handleSubmit = () => {
    form.reset();
    router.push("/");
  };
  return (
    <>
      <div style={pageStyle}>
        <HeaderMenu />

        <Carousel sx={{}} mx="auto" withIndicators>
          <Carousel.Slide>
            <BackgroundImage maw={"100%"} src="/img/bg.jpg">
              <Center p="xl">
                <Text
                  fw={500}
                  ml={50}
                  mt={600}
                  mb={100}
                  mr={400}
                  color="#ffff"
                  size="xl">
                  <Highlight
                    highlightColor="dark"
                    highlight="THIS IS A PLACE WHERE TECHNOLOGY & CREATIVITY FUSED INTO DIGITAL CHEMISTRY">
                    THIS IS A PLACE WHERE TECHNOLOGY & CREATIVITY FUSED INTO
                    DIGITAL CHEMISTRY
                  </Highlight>
                </Text>
              </Center>
            </BackgroundImage>
          </Carousel.Slide>
          <Carousel.Slide>
            <BackgroundImage maw={"100%"} src="/img/about-bg.jpg">
              <Center p="xl">
                <Text
                  fw={500}
                  mt={600}
                  mb={100}
                  mr={400}
                  color="#ffff"
                  size="xl">
                  <Highlight
                    highlightColor="dark"
                    highlight="WE DONT HAVE THE BEST BUT WE HAVE THE GREATEST TEAM">
                    WE DONT HAVE THE BEST BUT WE HAVE THE GREATEST TEAM
                  </Highlight>
                </Text>
              </Center>
            </BackgroundImage>
          </Carousel.Slide>
        </Carousel>
        <Container size="xl">
          <Center>
            <Title mt={70} order={2}>
              OUR VALUES
            </Title>
          </Center>

          <br />
          <Flex
            mr={150}
            ml={150}
            direction={{ base: "column", sm: "row" }}
            gap={{ base: "sm", sm: "lg" }}
            justify={{ sm: "center" }}>
            <Card
              style={cardStyle}
              shadow="sm"
              padding="lg"
              radius="md"
              withBorder>
              <center>
                <Image
                  mt={15}
                  alt="#"
                  maw={15}
                  src="/img/lightbulb-o.png"></Image>
              </center>
              <Title mb={20} align="center" color="white" order={4}>
                INNOVATIVE
              </Title>
              <Text mb={15} size="sm" align="center" color="white">
                Lorem ipsum dolor sit amet, consectur adpucingelit, Maxime
                eihfsdhfsdofhsdo sdfhsdofh sdfsdf , uncle, eque ipsa?
              </Text>
            </Card>
            <Card
              style={cardStyle2}
              shadow="sm"
              padding="lg"
              radius="md"
              withBorder>
              <center>
                <Image mt={15} alt="#" maw={25} src="/img/bank.png"></Image>
              </center>
              <Title mb={20} align="center" color="white" order={4}>
                LOYALTY
              </Title>
              <Text size="sm" align="center" color="white">
                Lorem ipsum dolor sit amet, consectur adpucingelit, Maxime
                eihfsdhfsdofhsdo sdfhsdofh sdfsdf , uncle, eque ipsa?
              </Text>
            </Card>
            <Card
              style={cardStyle3}
              shadow="sm"
              padding="lg"
              radius="md"
              withBorder>
              <center>
                <Image
                  alt="#"
                  mt={15}
                  maw={25}
                  src="/img/balance-scale.png"></Image>
              </center>
              <Title mb={20} align="center" color="white" order={4}>
                RESPECT
              </Title>
              <Text mb={15} size="sm" align="center" color="white">
                Lorem ipsum dolor sit amet, consectur adpucingelit, Maxime
                eihfsdhfsdofhsdo sdfhsdofh sdfsdf , uncle, eque ipsa?
              </Text>
            </Card>
          </Flex>
        </Container>
        <Container mt={70} size="xl">
          <Center>
            <Title order={3}>CONTACT US</Title>
          </Center>

          <Box maw={500} mx="auto" mb={100}>
            <form onSubmit={form.onSubmit(handleSubmit)}>
              <TextInput
                label="Name"
                placeholder=""
                {...form.getInputProps("name")}
              />
              <TextInput
                label="Email"
                placeholder=""
                {...form.getInputProps("email")}
              />

              <Textarea
                placeholder=""
                label="Message"
                {...form.getInputProps("message")}
              />

              <Button mt={10} type="submit" fullWidth>
                Submit
              </Button>
            </form>
          </Box>
        </Container>

        <FooterMenu />
      </div>
    </>
  );
}

import {
  createStyles,
  Container,
  Group,
  ActionIcon,
  rem,
  Text,
  Center,
  Image,
} from "@mantine/core";

const useStyles = createStyles((theme) => ({
  footer: {
    marginTop: rem(120),
  },
  inner: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    flexDirection: "column",
    paddingTop: theme.spacing.xl,
    paddingBottom: theme.spacing.xl,

    [theme.fn.smallerThan("xs")]: {
      flexDirection: "column",
    },
  },
  links: {
    display: "flex",
    alignItems: "center",
    marginTop: theme.spacing.md,

    [theme.fn.smallerThan("xs")]: {
      marginTop: theme.spacing.sm,
    },
  },
  linkIcon: {
    "&:hover": {
      color: "#3F2661",
      backgroundColor: "#E0DAD1",
    },
    marginRight: theme.spacing.sm,
  },
}));

export default function FooterMenu() {
  const { classes } = useStyles();

  return (
    <div
      className={classes.footer}
      style={{ backgroundColor: "#000000", margin: 0, border: "none" }}>
      <Container className={classes.inner}>
        <Text color="#FFFFFF" size="sm">
          Copyright © 2016 PT Company.
        </Text>
        <Group spacing="xs" className={classes.links}>
          <Image maw={20} src="/img/facebook-official.png" />
          <Image maw={20} src="/img/twitter.png" />
        </Group>
      </Container>
    </div>
  );
}

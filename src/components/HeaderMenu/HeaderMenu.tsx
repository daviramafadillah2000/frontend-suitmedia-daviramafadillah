import {
  createStyles,
  Header,
  Group,
  Button,
  Box,
  rem,
  Menu,
  Title,
} from "@mantine/core";

import "react-toastify/dist/ReactToastify.css";

import Link from "next/link";

const useStyles = createStyles((theme) => ({
  inner: {
    height: rem(56),
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
  },
  links: {
    [theme.fn.smallerThan("sm")]: {
      display: "none",
    },
  },
  burger: {
    [theme.fn.largerThan("sm")]: {
      display: "none",
    },
  },
  link: {
    display: "block",
    lineHeight: 1,
    padding: `${rem(8)} ${rem(12)}`,
    borderRadius: theme.radius.sm,
    textDecoration: "none",
    color:
      theme.colorScheme === "dark"
        ? theme.colors.dark[0]
        : theme.colors.gray[7],
    fontSize: theme.fontSizes.sm,
    fontWeight: 500,
    "&:hover": {
      color: theme.colors.white,
      backgroundColor: theme.colors.gray[4],
    },
  },

  linkLabel: {
    marginRight: rem(5),
  },
  hiddenMobile: {
    [theme.fn.smallerThan("sm")]: {
      display: "none",
    },
  },
  showMobile: {
    [theme.fn.smallerThan("sm")]: {
      display: "flex",
      flexDirection: "row",
      fontSize: "14px",
      margin: 0,
      padding: 0,
    },
  },
  hiddenDesktop: {
    [theme.fn.largerThan("sm")]: {
      display: "none",
    },
  },
}));

export default function HeaderMenu() {
  const { classes, theme } = useStyles();

  return (
    <Box>
      <Header
        height={60}
        px="md"
        style={{ backgroundColor: "#ffffff", border: "none" }}>
        <div className={classes.inner}>
          <Title order={4}>COMPANY</Title>
          <Group position="right" sx={{ height: "100%" }}>
            <Group
              sx={{ height: "100%" }}
              className={classes.showMobile}
              mr={80}>
              <Menu
                trigger="hover"
                openDelay={100}
                closeDelay={400}
                shadow="md"
                width={200}>
                <Menu.Target>
                  <Button
                    sx={{ backgroundColor: "#FFFFFF" }}
                    radius="xs"
                    className={classes.link}>
                    ABOUT
                  </Button>
                </Menu.Target>
                <Menu.Dropdown>
                  <Menu.Item component={Link} href="#">
                    HISTORY
                  </Menu.Item>
                  <Menu.Item component={Link} href="#">
                    VISION MISSION
                  </Menu.Item>
                </Menu.Dropdown>
              </Menu>
              <Link href="/" className={classes.link}>
                OUR WORK
              </Link>
              <Link href="/" className={classes.link}>
                OUR TEAM
              </Link>
              <Link href="/" className={classes.link}>
                CONTACT
              </Link>
            </Group>
          </Group>
        </div>
      </Header>
    </Box>
  );
}
